import com.thingworx.entities.utils.EntityUtilities;
import com.thingworx.logging.LogUtilities;
import com.thingworx.metadata.annotations.ThingworxServiceDefinition;
import com.thingworx.metadata.annotations.ThingworxServiceParameter;
import com.thingworx.metadata.annotations.ThingworxServiceResult;
import com.thingworx.relationships.RelationshipTypes.ThingworxRelationshipTypes;
import com.thingworx.resources.Resource;
import com.thingworx.resources.entities.EntityServices;
import com.thingworx.things.Thing;
import com.thingworx.things.repository.FileRepositoryThing;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.StringPrimitive;

import java.awt.image.*;
import org.apache.pdfbox.tools.PDFToImage;
import org.slf4j.Logger;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.pdfbox.tools.*; 

public class SIP_StationsCoordinator_Resource extends Resource {

	//initialize logger
	private static Logger _logger = LogUtilities.getInstance()
			.getApplicationLogger(SIP_StationsCoordinator_Resource.class);
	public static String mypdfFileName = "";

	public SIP_StationsCoordinator_Resource() {
		// TODO Auto-generated constructor stub
	}
	/**
	This service create a station from the pdf file name
	create media enetiy for the station
	create the station thing
	 */
	@ThingworxServiceDefinition(name = "CreateStation", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "NOTHING", aspects = {})
	public void CreateStation(
			@ThingworxServiceParameter(name = "pdfFileName", description = "name of the pdf file in the repository, without the .pdf extension", baseType = "STRING", aspects = {
					"isRequired:true" }) String pdfFileName,
			@ThingworxServiceParameter(name = "DPI", description = "DPI of the Image will generated from the PDF", baseType = "STRING", aspects = {
			"isRequired:true" }) String DPI
			) throws Exception {
		try {
			// **make the background transparent**
			_logger.trace("Entering Service: CreateStation");
			this.mypdfFileName = pdfFileName;
			String[] args = new String[3];

			// Search for and cast the FileRepository Thing from the server.
			FileRepositoryThing ft = (FileRepositoryThing) EntityUtilities.findEntity("SystemRepository",
					ThingworxRelationshipTypes.Thing);

			// Get the path to the root of the file repository (no trailing slashes):
			String rootPath = ft.getRootPath();

			// Append any sub-folders (in the repository) and the filename to the path.
			String fullFilePath = rootPath + "/" + pdfFileName + ".pdf";

			args[0] = fullFilePath;
			args[1] = "-dpi";
			args[2] = DPI; // 96 will not work well, 72 is good

			PDFToImage.main(args);

			// **make the background transparent**
			// here we added 1 because the library adds it
			File file = new File(rootPath + "/" + pdfFileName + "1" + ".jpg");
			BufferedImage img = ImageIO.read(file);
			Image img2 = ImageIO.read(file);

			BufferedImage TransparentImage = rasterToAlpha(img, Color.WHITE);

			File outputFile = new File(rootPath + "/" + pdfFileName + "11" + ".png");
			ImageIO.write(TransparentImage, "png", outputFile);

			EntityServices entityService = (EntityServices) EntityUtilities.findEntity("EntityServices",
					ThingworxRelationshipTypes.Resource);

			entityService.CreateMediaEntity("SIP_" + pdfFileName + "_Media", null, null, ImageToBytes(TransparentImage));
			//entityService.UpdateMediaEntity("SIP_" + pdfFileName + "_Media", ImageToBytes(TransparentImage));

			//Create station Thing
			Thing siteManagement_masterThing = (Thing) EntityUtilities.findEntity("DVM_SiteManagement_MasterThing",
					ThingworxRelationshipTypes.Thing);

			ValueCollection values = new ValueCollection();
			values.put("stationName", new StringPrimitive(pdfFileName));                                                                                                                                                                                                                          
            values.put("floorPlanBackground", new StringPrimitive("SIP_" + pdfFileName + "_Media"));
            values.put("thingName", new StringPrimitive("SIP_" + pdfFileName + "_Thing"));
////            values.put("SOSCamera1", new StringPrimitive(""));
////            values.put("SOSCamera2", new StringPrimitive(""));
////            values.put("VDGServerIP", new StringPrimitive(""));
				
			//create the station thing by calling CreateStataion service in the DVM_SiteManagement_MasterThing entity
			siteManagement_masterThing.processAPIServiceRequest("CreateStation",values);
			
			
			String[] args2 = new String[1];
			args2[0] = fullFilePath;
			//call to adjust the icons coordinates
			PDfExtractor.main(args2);
			
			_logger.trace("Exiting Service: CreateStation");
			}
			catch(Exception e)
			{
				_logger.error("Stations Coordinator Extension Error: " +e.getMessage());
				throw e;
			}
		 
	}
	
	
	
	
	
	
	
	@SuppressWarnings("finally")
	public byte[] ImageToBytes(BufferedImage originalImage) {
		byte[] imageInBytes = null;
		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "png", baos);
			baos.flush();
			imageInBytes = baos.toByteArray();
			baos.close();

		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			return imageInBytes;
		}

	}
	
	
	
	 //change the white color to be transparenst (as I remembered)
	 public BufferedImage rasterToAlpha(BufferedImage sourceImage, Color origColor) {

		     
	        BufferedImage targetImage = new BufferedImage(sourceImage.getWidth(),
	                sourceImage.getHeight(),
	                BufferedImage.TYPE_4BYTE_ABGR);
	       
	        WritableRaster targetRaster = targetImage.getRaster();
	         
	        WritableRaster sourceRaster = sourceImage.getRaster();

	        for (int row = 0; row < sourceImage.getHeight(); row++) {
 
	            int[] rgba = new int[4 * sourceImage.getWidth()];
	            
	            int[] rgb = new int[3 * sourceImage.getWidth()];

	            
	            // Get the next row of pixels
	            sourceRaster.getPixels(0, row, sourceImage.getWidth(), 1, rgb);

	            for (int i = 0, j = 0; i < rgb.length; i += 3, j += 4) {
		           // _logger.warn("Before if");
	                if (origColor.equals(new Color(rgb[i], rgb[i + 1], rgb[i + 2]))) {
	                    // If it's the same make it transparent
	                    rgba[j] = 0;
	                    rgba[j + 1] = 0;
	                    rgba[j + 2] = 0;
	                    rgba[j + 3] = 0;
	                } else {
	                    rgba[j] = rgb[i];
	                    rgba[j + 1] = rgb[i + 1];
	                    rgba[j + 2] = rgb[i + 2];
	                    // Make it opaque
	                    rgba[j + 3] = 255;
	                }
	            }
	            // Write the line
	            
	            targetRaster.setPixels(0, row, sourceImage.getWidth(), 1, rgba);
	        }
	        return targetImage;
	    }

	
	//adjust the icon coordinates
	
	@ThingworxServiceDefinition(name = "UpdateCoordinations", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "NOTHING", aspects = {})
	public void UpdateCoordinations(
			@ThingworxServiceParameter(name = "pdfFileName", description = "", baseType = "STRING") String pdfFileName,
			@ThingworxServiceParameter(name = "DPI", description = "DPI of the Image will generated from the PDF", baseType = "STRING", aspects = {
			"isRequired:true" }) String DPI) throws Exception {
		
		
		_logger.trace("Entering Service: UpdateCoordinations");
		
		

		this.mypdfFileName = pdfFileName;
		String[] args = new String[3];

		// Search for and cast the FileRepository Thing from the server.
		FileRepositoryThing ft = (FileRepositoryThing) EntityUtilities.findEntity("SystemRepository",
				ThingworxRelationshipTypes.Thing);

		// Get the path to the root of the file repository (no trailing slashes):
		String rootPath = ft.getRootPath();

		// Append any sub-folders (in the repository) and the filename to the path.
		String fullFilePath = rootPath + "/" + pdfFileName + ".pdf";

		args[0] = fullFilePath;
		args[1] = "-dpi";
		args[2] = DPI; // 96 will not work well, 72 is good

		PDFToImage.main(args);

		// **make the background transparent (instead of white)**
		// here we added 1 because the library adds it
		File file = new File(rootPath + "/" + pdfFileName + "1" + ".jpg");
		BufferedImage img = ImageIO.read(file);
		Image img2 = ImageIO.read(file);

		BufferedImage TransparentImage = rasterToAlpha(img, Color.WHITE);

		File outputFile = new File(rootPath + "/" + pdfFileName + "11" + ".png");
		ImageIO.write(TransparentImage, "png", outputFile);

		EntityServices entityService = (EntityServices) EntityUtilities.findEntity("EntityServices",
				ThingworxRelationshipTypes.Resource);

	//	entityService.CreateMediaEntity("SIP_" + pdfFileName + "_Media", null, null, ImageToBytes(TransparentImage));
		entityService.UpdateMediaEntity("SIP_" + pdfFileName + "_Media", ImageToBytes(TransparentImage));

		//Create station Thing
//		Thing siteManagement_masterThing = (Thing) EntityUtilities.findEntity("DVM_SiteManagement_MasterThing",
//				ThingworxRelationshipTypes.Thing);
//
//		ValueCollection values = new ValueCollection();
//		values.put("stationName", new StringPrimitive(pdfFileName));
//        values.put("floorPlanBackground", new StringPrimitive("SIP_" + pdfFileName + "_Media"));
//        values.put("thingName", new StringPrimitive("SIP_" + pdfFileName + "_Thing"));
////        values.put("SOSCamera1", new StringPrimitive(""));
////        values.put("SOSCamera2", new StringPrimitive(""));
////        values.put("VDGServerIP", new StringPrimitive(""));
//
//		siteManagement_masterThing.processAPIServiceRequest("CreateStation",values);
		
		

		
		
		_logger.trace("Exiting Service: UpdateCoordinations");
	}
	/*(optional) update the floor plan media entity and put an image without icons : 
	if you have the pdf without icons on ("system repository")
	after you finished the icons coordinates adjustments
	you can call this method to update the media entity and 
	put an image without icons.
	this service reads the pdf file, converts it to image
	and updates the media entity of the floor plan image
	
	*/
	@ThingworxServiceDefinition(name = "RemoveFloorPlanIcons", description = "", category = "", isAllowOverride = false, aspects = {
	"isAsync:false" })
		@ThingworxServiceResult(name = "Result", description = "", baseType = "NOTHING", aspects = {})
	public void RemoveFloorPlanIcons(
			@ThingworxServiceParameter(name = "pdfFileName", description = "", baseType = "STRING") String pdfFileName,
			@ThingworxServiceParameter(name = "DPI", description = "DPI of the Image will generated from the PDF", baseType = "STRING", aspects = {
					"isRequired:true" }) String DPI) throws Exception {


		_logger.trace("Entering Service: RemoveFloorPlanIcons");



		this.mypdfFileName = pdfFileName;
		String[] args = new String[3];

		// Search for and cast the FileRepository Thing from the server.
		FileRepositoryThing ft = (FileRepositoryThing) EntityUtilities.findEntity("SystemRepository",
				ThingworxRelationshipTypes.Thing);

		// Get the path to the root of the file repository (no trailing slashes):
		String rootPath = ft.getRootPath();

		// Append any sub-folders (in the repository) and the filename to the path.
		String fullFilePath = rootPath + "/" + pdfFileName + ".pdf";

		args[0] = fullFilePath;
		args[1] = "-dpi";
		args[2] = DPI; // 96 will not work well, 72 is good

		PDFToImage.main(args);

		// **make the background transparent**
		// here we added 1 because the library adds it
		File file = new File(rootPath + "/" + pdfFileName + "1" + ".jpg");
		BufferedImage img = ImageIO.read(file);
		Image img2 = ImageIO.read(file);

		BufferedImage TransparentImage = rasterToAlpha(img, Color.WHITE);

		File outputFile = new File(rootPath + "/" + pdfFileName + "11" + ".png");
		ImageIO.write(TransparentImage, "png", outputFile);

		EntityServices entityService = (EntityServices) EntityUtilities.findEntity("EntityServices",
				ThingworxRelationshipTypes.Resource);

		//	entityService.CreateMediaEntity("SIP_" + pdfFileName + "_Media", null, null, ImageToBytes(TransparentImage));


		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(pdfFileName);
		m.find();
		String mediaNumber = m.group(0);

		entityService.UpdateMediaEntity("SIP_" + mediaNumber + "_Media", ImageToBytes(TransparentImage));

		//Create station Thing
//		Thing siteManagement_masterThing = (Thing) EntityUtilities.findEntity("DVM_SiteManagement_MasterThing",
//				ThingworxRelationshipTypes.Thing);
//
//		ValueCollection values = new ValueCollection();
//		values.put("stationName", new StringPrimitive(pdfFileName));
//        values.put("floorPlanBackground", new StringPrimitive("SIP_" + pdfFileName + "_Media"));
//        values.put("thingName", new StringPrimitive("SIP_" + pdfFileName + "_Thing"));
////        values.put("SOSCamera1", new StringPrimitive(""));
////        values.put("SOSCamera2", new StringPrimitive(""));
////        values.put("VDGServerIP", new StringPrimitive(""));
//
//		siteManagement_masterThing.processAPIServiceRequest("CreateStation",values);





		_logger.trace("Exiting Service: RemoveFloorPlanIcons");
	}
	 
	
	
	

}
