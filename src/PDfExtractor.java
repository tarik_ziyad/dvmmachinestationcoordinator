/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.thingworx.types.primitives.StringPrimitive;
import com.thingworx.data.util.InfoTableInstanceFactory;
import com.thingworx.entities.utils.EntityUtilities;
import com.thingworx.logging.LogUtilities;
import com.thingworx.relationships.RelationshipTypes.ThingworxRelationshipTypes;
import com.thingworx.resources.entities.EntityServices;
import com.thingworx.resources.queries.InfoTableFunctions;
import com.thingworx.things.Thing;
import com.thingworx.things.repository.FileRepositoryThing;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;

/**
 * This is an example on how to get some x/y coordinates of text.
 *
 * @author Ben Litchfield
 */
public class PDfExtractor extends PDFTextStripper {
	
	private static Logger _logger = LogUtilities.getInstance()
			.getApplicationLogger(SIP_StationsCoordinator_Resource.class);
	/**
	 * Instantiate a new PDFTextStripper object.
	 *
	 * @throws IOException If there is an error loading the properties.
	 */
	public PDfExtractor() throws IOException {
	}

	/**
	 * This will print the documents data.
	 *
	 * @param args The command line arguments.
	 *
	 * @throws IOException If there is an error parsing the document.
	 */
	public static void main(String[] args) throws IOException {

		// Search for and cast the FileRepository Thing from the server.
		FileRepositoryThing ft = (FileRepositoryThing) EntityUtilities.findEntity("SystemRepository",
				ThingworxRelationshipTypes.Thing);

		// Get the path to the root of the file repository (no trailing slashes):
		String rootPath = ft.getRootPath();

		// Append any sub-folders (in the repository) and the filename to the path.
		String fullFilePath = rootPath + "/" + SIP_StationsCoordinator_Resource.mypdfFileName + ".pdf";

		args = new String[1];
		args[0] = fullFilePath;


		//args[0] = SIP_StationsCoordinator_Resource.mypdfFileName;
		if (args.length != 1) {
			usage();
		} else {
			PDDocument document = null;
			try {
				document = PDDocument.load(new File(args[0]));

				PDFTextStripper stripper = new PDfExtractor();
				stripper.setSortByPosition(true);
				stripper.setStartPage(0);
				stripper.setEndPage(document.getNumberOfPages());

				Writer dummy = new OutputStreamWriter(new ByteArrayOutputStream());
				stripper.writeText(document, dummy);
			} finally {
				if (document != null) {
					document.close();
				}
			}
		}
	}

	/**
	 * Override the default functionality of PDFTextStripper.
	 */
	// string represents the found word in PDF file
	@Override
	protected void writeString(String string, List<TextPosition> textPositions) throws IOException {
		try {
			String cameraPattern =  ".*[Cc][Aa][Mm]-?(\\d+).*";
			String bayPattern_TW = ".*[Bb][Aa][Yy]-?(\\d+).*";
			String bayPattern_pdf = ".*[Ss]-?(\\d+).*";

			String thingName = "SIP_" + SIP_StationsCoordinator_Resource.mypdfFileName + "_Thing";

			Thing stationThing = (Thing) EntityUtilities.findEntity(thingName,
					ThingworxRelationshipTypes.Thing);

			InfoTable floorplanOriginal = (InfoTable) stationThing.getPropertyValue("floorPlanData").getValue();
			InfoTable floorplanEdited = floorplanOriginal;

			int floorplanLength = floorplanOriginal.getLength();
			for (int i = 0; i < floorplanLength; i++) {
				ValueCollection row = floorplanOriginal.getRow(i);
				String metaDate = row.getValue("MetaData").toString();

				if (metaDate.matches(cameraPattern) && string.matches(cameraPattern)) {
					// compare the number of tow cams
					Pattern p = Pattern.compile(cameraPattern);
					Matcher m = p.matcher(metaDate);
					m.find();
					int camNumber_metaData = Integer.parseInt(m.group(1));

					Matcher m3 = p.matcher(string);
					m3.find();
					int  camNumber_string = Integer.parseInt(m3.group(1));


					if (camNumber_metaData == camNumber_string) {
						// assign the coordinations
						InfoTableFunctions InfoTableFunctions = (InfoTableFunctions) EntityUtilities
								.findEntity("InfoTableFunctions", ThingworxRelationshipTypes.Resource);
						InfoTable infoTableRow = InfoTableInstanceFactory
								.createInfoTableFromDataShape("FloorPlanDataShape");
						row.SetNumberValue("PositionX", textPositions.get(0).getXDirAdj());
						row.SetNumberValue("PositionY", textPositions.get(0).getYDirAdj());
						
						JSONObject query = new JSONObject();
						JSONObject filters = new JSONObject();
						filters.put("fieldName", "MetaData");
						filters.put("type", "EQ");
						filters.put("value", metaDate);
						query.put("filters", filters);

						floorplanEdited.setRow(i, row);
                        floorplanOriginal.removeRow(i);
                        floorplanOriginal.addRow(row);
						//InfoTableFunctions.UpdateQuery(floorplanOriginal, query, floorplanEdited);

					}
				} else if (metaDate.matches(bayPattern_TW) && string.matches(bayPattern_pdf)) {
					
					// compare the number of tow cams
					Pattern p = Pattern.compile(bayPattern_TW);
					Matcher m = p.matcher(metaDate);
					m.find();
					int bayNumber_metaData = Integer.parseInt(m.group(1));

					Pattern p2 = Pattern.compile(bayPattern_pdf);
					Matcher m3 = p2.matcher(string);
					m3.find();
					int bayNumber_string = Integer.parseInt(m3.group(1));
					
					if (bayNumber_metaData == bayNumber_string) {
						// assign the coordinations
						InfoTableFunctions InfoTableFunctions = (InfoTableFunctions) EntityUtilities
								.findEntity("InfoTableFunctions", ThingworxRelationshipTypes.Resource);
						InfoTable infoTableRow = InfoTableInstanceFactory
								.createInfoTableFromDataShape("FloorPlanDataShape");
						row.SetNumberValue("PositionX", textPositions.get(0).getXDirAdj());
						row.SetNumberValue("PositionY", textPositions.get(0).getYDirAdj());
						
						JSONObject query = new JSONObject();
						JSONObject filters = new JSONObject();
						filters.put("fieldName", "MetaData");
						filters.put("type", "EQ");
						filters.put("value", metaDate);
						query.put("filters", filters);


//						floorplanEdited.setRow(i, row);
//                        floorplanOriginal.removeRow(i);
//                        floorplanOriginal.addRow(row);
                        floorplanOriginal.setRow(i, row);
						//InfoTableFunctions.UpdateQuery(floorplanOriginal, query, floorplanEdited);

					}
					

				}

			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			 
		}

//    	if(string.matches(pattern))
//    		System.out.println(string );
//    	ValueCollection values = new ValueCollection();
//		values.put("ServiceKeys", new StringPrimitive(service));
//		dataInfoTable.addRow(values);

		// System.out.println(string.replaceAll(pattern, "$1$3"));
		// System.out.println(string + " " + textPositions.get(0).getXDirAdj() + "," +
		// textPositions.get(0).getYDirAdj() );
//        for (TextPosition text : textPositions)
//        {
//            System.out.println(string + " String[" + text.getXDirAdj() + "," +
//                    text.getYDirAdj() + " fs=" + text.getFontSize() + " xscale=" +
//                    text.getXScale() + " height=" + text.getHeightDir() + " space=" +
//                    text.getWidthOfSpace() + " width=" +
//                    text.getWidthDirAdj() + "]" + text.getUnicode() );
//        }

	}

	/**
	 * This will print the usage for this document.
	 */
	private static void usage() {
		System.err.println("Usage: java " + PDfExtractor.class.getName() + " <input-pdf>");
	}
}
